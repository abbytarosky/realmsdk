package sdk.realm;

import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import sdk.realm.Fragments.Capture;
import sdk.realm.Fragments.Home;
import sdk.realm.Fragments.Notifications;
import sdk.realm.Fragments.Profile;
import sdk.realm.Fragments.Search;

public class Skeleton extends AppCompatActivity {

    Fragment notif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skeleton);

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));

        notif = new Notifications();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //Set the tab ICONS
        tabLayout.getTabAt(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_friends));
        tabLayout.getTabAt(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_search));
        tabLayout.getTabAt(2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_capture));
        tabLayout.getTabAt(3).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notifications));
        tabLayout.getTabAt(4).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_profile));

    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //get which view is selected
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position)
            {
                case 3:
                    return notif;
            }
            return notif;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
