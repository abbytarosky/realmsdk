package sdk.realm;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.*;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Semaphore;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import static android.content.Context.CAMERA_SERVICE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by joelv on 9/16/2017.
 */
@TargetApi(21)
public class CameraHandler implements TextureView.SurfaceTextureListener, ImageReader.OnImageAvailableListener{

    public enum Mode{
        REGULAR,
        SELFIE
    }

    private Mode mode = Mode.REGULAR;
    private Bitmap capture;
    private String cameraId = null;
    private CameraManager manager;
    private CameraDevice device;
    private ImageReader reader;
    private TextureView surfaceView;
    private ImageView imageView;
    private Activity activity;
    private Surface surface;
    private Semaphore cameraOpenCloseLock;
    private CaptureRequest.Builder previewBuilder, captureBuilder;
    private CameraCaptureSession captureSession;
    private HandlerThread backgroundThread;
    private Handler backgroundHandler;
    private int autoflash = CameraMetadata.CONTROL_AE_MODE_ON;
    private int autofocus = CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE;
    private int rotation;
    private Size previewSize = null;
    private boolean switching = false;

    private CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback(){
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                @NonNull CaptureRequest request,
                @NonNull TotalCaptureResult result){
            updatePreview();
        }

    };

    private CameraDevice.StateCallback callback = new CameraDevice.StateCallback(){
        @Override
        public void onOpened(@NonNull CameraDevice camera){
            cameraOpenCloseLock.release();
            device = camera;
            createRequestBuilders();
            createSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera){
            cameraOpenCloseLock.release();
            device.close();
            device = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error){
            cameraOpenCloseLock.release();
            device.close();
            device = null;
        }
    };

    public CameraHandler(TextureView view, ImageView img, Activity activity){
        this.activity = activity;
        this.surfaceView = view;
        this.imageView = img;
        this.cameraOpenCloseLock = new Semaphore(1);
        this.surfaceView.setSurfaceTextureListener(this);
        this.manager = (CameraManager)activity.getSystemService(CAMERA_SERVICE);
    }

    @Override
    public void onImageAvailable(ImageReader reader) {
        Image image = reader.acquireLatestImage();
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        Bitmap b = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
        capture = rotatedBitmap(b);//Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), transform, true);
        image.close();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageBitmap(capture);
                imageView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void openCamera(Mode mode){
        Integer lens = 0;
        if (mode==Mode.REGULAR){
            lens = CameraCharacteristics.LENS_FACING_BACK;
        }
        else{
            lens = CameraCharacteristics.LENS_FACING_FRONT;
        }
        try{
            for (String id : manager.getCameraIdList()){
                Integer test = manager.getCameraCharacteristics(id).get(CameraCharacteristics.LENS_FACING);
                if (test != null && test.equals(lens)){
                    cameraId = id;
                    break;
                }
            }
            if (cameraId==null) throw new Exception("Null camera ID");
            //Open a camera device
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PERMISSION_GRANTED) {
                manager.openCamera(cameraId, callback, null);
            }
            else{
                throw new Exception();
            }
            //if (ActivityCompat.checkSelfPermission(activity, ))
        }catch(Exception e){
            toast("openCamera: " + e.getMessage());
        }
    }

    private void createRequestBuilders(){
        try{
            configureSurfaces(cameraId);
            SurfaceTexture texture = surfaceView.getSurfaceTexture();
            assert texture!=null : "Texture is null";
            texture.setDefaultBufferSize(surfaceView.getWidth(), surfaceView.getHeight());
            this.surface = new Surface(texture);
            previewBuilder = device.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewBuilder.addTarget(surface);
            previewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            //Reader
            captureBuilder = device.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            //TODO: Support flash later

        }catch (Exception e){
            if (e.getClass()!=NullPointerException.class) toast("createRequestBuilders: " + e.getMessage());
        }
    }

    private void createSession(){
        try{
            device.createCaptureSession(Arrays.asList(surface, reader.getSurface()),
                    new CameraCaptureSession.StateCallback(){

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session){
                            if (device==null) return;
                            captureSession = session;
                            try {
                                updatePreview();
                                switching = false;
                            }catch(Exception e){
                                toast("onConfigured: " + e.getMessage());
                            }

                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session){
                            //Find a way to show error
                            toast("onConfigureFailed:");
                        }
                    }, null);
        }catch (Exception e){
            if (e.getClass()!=NullPointerException.class) toast("createSession: " + e.getMessage());
        }
    }

    private void updatePreview(){
        try {
            //TODO: add method to update the request parameters for flash
            captureSession.setRepeatingRequest(previewBuilder.build(), captureCallback, backgroundHandler);
        }catch(Exception e){
            toast("updatePreview: " + e.getMessage());
        }
    }

    public void takePicture(){
        try{
            captureSession.stopRepeating();
            captureSession.capture(captureBuilder.build(), captureCallback, null);
        }catch(Exception e){
            toast("takePicture: " + e.getMessage());
        }
    }

    public void closeCamera(){
        try{
            cameraOpenCloseLock.acquire();
            captureSession.close();
            captureSession = null;
            device.close();
            device = null;
            reader.close();
            reader = null;
            surface.release();
            surface = null;
        }catch(Exception e){
            toast("closeCamera: " + e.getMessage());
        }finally{
            cameraOpenCloseLock.release();
        }
    }

    private void startBackgroundThread(){

        backgroundThread = new HandlerThread("Camera");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    private void stopBackgroundThread(){
        try{
            backgroundThread.quitSafely();
            backgroundThread.join();
            backgroundThread = null;
            backgroundHandler = null;
        }catch(Exception e){
            toast("stopBackgroundThread: " + e.getMessage());
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height){
        startBackgroundThread();
        openCamera(mode);
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height){
        toast(width + " x " + height);
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture){
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface){
        closeCamera();
        stopBackgroundThread();
        return true;
    }

    private void configureSurfaces(String id){
        try{
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(id);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map==null) throw new Exception("No map");
            previewSize = map.getOutputSizes(ImageFormat.JPEG)[0];
            reader = ImageReader.newInstance(previewSize.getWidth(), previewSize.getHeight(), ImageFormat.JPEG, 1);
            reader.setOnImageAvailableListener(this, backgroundHandler);
            configurePreview();
        }catch(Exception e){
            toast("configureImageReader: " + e.getMessage());
        }
    }

    public void switchCamera(){
        switching = true;
        closeCamera();
        mode = mode==Mode.REGULAR ? Mode.SELFIE : Mode.REGULAR;
        openCamera(mode);
    }

    public Bitmap getBitmap(){
        return this.capture;
    }

    private Bitmap rotatedBitmap(Bitmap b){
        //TODO: Scale bitmap to match image preview
        Matrix m = new Matrix();
        m.postRotate((90 * rotation + 90) % 360);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    private void toast(String msg){
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    public int getAutoflash(){
        return autoflash;
    }

    public int getAutofocus(){
        return autofocus;
    }

    public boolean isSwitching(){
        return switching;
    }

    private void configurePreview(){
        if (surfaceView == null || previewSize == null){
            toast("configureTransform: Null");
            return;
        }
        rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        boolean portrait = rotation % 2 == 0;
        Matrix m = new Matrix();
        RectF viewRect = new RectF(0, 0, surfaceView.getWidth(), surfaceView.getHeight());
        float centerX = viewRect.centerX(), centerY = viewRect.centerY();
        if (!portrait){
            RectF camRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());
            camRect.offset(centerX - camRect.centerX(), centerY - camRect.centerY());
            m.setRectToRect(viewRect, camRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max((float) surfaceView.getHeight() / previewSize.getHeight(),
                    (float) surfaceView.getWidth() / previewSize.getWidth());
            m.postScale(scale, scale, centerX, centerY);
        }
        m.postRotate(90 * rotation, centerX, centerY);
        surfaceView.setTransform(m);
    }

    public void clearBitmap(){
        capture = null;
    }
}
