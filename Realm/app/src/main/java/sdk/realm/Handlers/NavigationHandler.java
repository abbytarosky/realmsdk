package sdk.realm.Handlers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import sdk.realm.Fragments.Capture;
import sdk.realm.Fragments.Home;
import sdk.realm.Fragments.Profile;
import sdk.realm.Fragments.Search;
import sdk.realm.R;

/**
 * Created by Abby on 11/27/2017.
 */


public class NavigationHandler extends View {

    Context c;

    ImageView map;
    ImageView search;
    ImageView profile;
    ImageView capture;
    ImageView notific;

    public NavigationHandler(Context context) {
        super(context);
        c = context;
    }

    public void SetUp(View v)
    {
        if(v == null)
        {
            Log.e("NAV", "Cannot find Navigation View");
            return;
        }
        map = (ImageView)v.findViewById(R.id.map);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                getContext().startActivity(i);
            }
        });

        search = (ImageView)v.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), Search.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                getContext().startActivity(i);
            }
        });

        profile = (ImageView)v.findViewById(R.id.profile);
        profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), Profile.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                getContext().startActivity(i);
            }
        });

        capture = (ImageView)v.findViewById(R.id.capture);
        capture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), Capture.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                getContext().startActivity(i);
            }
        });
    }

    public void SetMap()
    {
        if(map!=null)
            map.setColorFilter(ContextCompat.getColor(c, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
    }
    public void SetSearch()
    {
        if(search!=null)
            search.setColorFilter(ContextCompat.getColor(c, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void SetProfile()
    {
        if(profile!=null)
            profile.setColorFilter(ContextCompat.getColor(c, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void SetCapture()
    {
        if(capture!=null)
            capture.setColorFilter(ContextCompat.getColor(c, R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);
    }
}