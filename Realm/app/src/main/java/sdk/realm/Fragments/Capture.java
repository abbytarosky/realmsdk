package sdk.realm.Fragments;

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.params.BlackLevelPattern;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.SupportMapFragment;

import sdk.realm.CameraHandler;
import sdk.realm.Handlers.NavigationHandler;
import sdk.realm.R;

@TargetApi(21)
public class Capture extends AppCompatActivity{

    private TextureView view;
    private ImageView image;
    private CameraHandler camera;
    private Button takePicture, switchCamera, cancelButton;
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_capture);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));


        NavigationHandler navigationHandler = new NavigationHandler(this);
        navigationHandler.SetUp(findViewById(R.id.nav));
        navigationHandler.SetCapture();

        view = (TextureView)findViewById(R.id.texture);
        image = (ImageView)findViewById(R.id.img);
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        camera = new CameraHandler(view, image, this);
        camera.openCamera(CameraHandler.Mode.REGULAR);
        takePicture = (Button)findViewById(R.id.picture);

        takePicture.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //Toast.makeText(getActivity(), "Clicking", Toast.LENGTH_LONG).show();
                //if (view.getId()==R.id.picture){
                camera.takePicture();
                displayImagePreview();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            }
        });

        switchCamera = (Button)findViewById(R.id.switchCamera);
        switchCamera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (!camera.isSwitching()) camera.switchCamera();
            }
        });

        cancelButton = (Button)findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayCameraPreview();
                camera.clearBitmap();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    public void onPause(){
        //stopBackgroundThread()
        super.onPause();

    }

    private void displayCameraPreview(){
        view.setVisibility(View.VISIBLE);
        switchCamera.setVisibility(View.VISIBLE);
        takePicture.setVisibility(View.VISIBLE);
        image.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);
    }

    private void displayImagePreview(){
        view.setVisibility(View.INVISIBLE);
        switchCamera.setVisibility(View.INVISIBLE);
        takePicture.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.VISIBLE);
    }

}
